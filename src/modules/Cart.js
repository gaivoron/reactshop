import React from 'react';

const Cart = function()
{
    return (
        <div id="cart">
            <div id="cart-header">
                quantity:10 / sum: 2500
            </div>
            <div id="cart-body">
                <table>
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Count</th>
                            <th>Sum</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Image</td>
                            <td>Product-1</td>
                            <td>5</td>
                            <td>2500</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}


export default Cart;