import React from 'react';
import Menu from '../modules/Menu';
import Cart from '../modules/Cart';

const Header = function(){
    return (
        <div>
            <Menu />
            <Cart />
        </div>
    )
}


export default Header;