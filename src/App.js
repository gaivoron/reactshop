import React from 'react';
import Header from  './static/Header';
import Home from    './pages/Home';
import Footer from  './static/Footer';

const App = function(){
    return (
        <div>
            <Header />
            <Home />
            <Footer />
        </div>
    )
}


export default App;